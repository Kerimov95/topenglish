import { useEffect, useRef, useState } from "react";


export const useSounds = () => {

    const [isLoading, setIsLoading] = useState(false);
    const [isError, setIsError] = useState(false);
    const [data, setData] = useState([]);
    const ref = useRef(new AbortController());

    const getSounds = async () => {
        setIsError(false);
        setIsLoading(true);
        fetch("http://localhost:3000/api/sounds", { signal: ref.current.signal }).then(async (s) => {
            const data = await s.json();
            setData(data);
        }).catch(() => {
            setIsError(true);
        }).finally(() => {
            setIsLoading(false);
        })
    };

    const update = () => {
        ref.current.abort();
        getSounds();
    }

    useEffect(() => {
        getSounds();

        return () => {
            ref.current.abort();
        }
    }, [])


    return { data, isLoading, isError, update }
}