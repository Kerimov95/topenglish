import React, { useEffect, useRef, useState } from "react";
import {
  Menu,
  Label,
  Container,
  Header,
  Segment,
  Grid,
  List,
  Input,
} from "semantic-ui-react";
import { useSounds } from "./hooks/useSounds";
import logo from "./assets/logo.jpg";
import "./app.css";
import "semantic-ui-css/semantic.min.css";

export const App = () => {
  const [activeItem, setactiveItem] = useState("");
  const { data, isLoading, isError, update } = useSounds();
  const [findNumber, setFindNumber] = useState("");
  const [currentSound, setCurrentSound] = useState();

  useEffect(() => {
    if (!isLoading && data?.length > 0) {
      setactiveItem(data[0].name);
    }
  }, [isLoading, isLoading]);

  return (
    <Container style={{ padding: "3px" }}>
      <Menu stackable>
        <Menu.Item>
          <img alt="logo" src={logo} />
        </Menu.Item>
        <Menu.Item name="features">
          <Header>TopEnglish</Header>
        </Menu.Item>
      </Menu>
      <Grid>
        <Grid.Column width={4}>
          <Menu vertical>
            <Menu.Item>
              <Input
                value={findNumber}
                onChange={(e) => setFindNumber(e.target.value)}
                icon="search"
                placeholder="Номер..."
              />
            </Menu.Item>
            {data.map((s) => (
              <Menu.Item
                key={s.name}
                name={s.name}
                active={activeItem === s.name}
                onClick={() => setactiveItem(s.name)}
              >
                <Label color="teal">{s.sounds?.length || 0}</Label>
                {s.name}
              </Menu.Item>
            ))}
          </Menu>
        </Grid.Column>
        <Grid.Column stretched width={12}>
          <Segment>
            <div>
              <Palyer sound={currentSound} />
            </div>
            <List divided relaxed>
              {data
                .find((l) => l.name === activeItem)
                ?.sounds?.filter((s) => s.name.includes(findNumber))
                .map((s) => (
                  <ItemRow
                    onClick={() => setCurrentSound(s)}
                    key={s.name}
                    sound={s}
                    active={s === currentSound}
                  />
                ))}
            </List>
          </Segment>
        </Grid.Column>
      </Grid>
    </Container>
  );
};

const Palyer = ({ sound }) => {
  const palyer = useRef();

  useEffect(() => {
    if (sound) {
      palyer?.current?.load();
      palyer?.current?.play();
    }
  }, [sound]);

  return (
    <Segment>
      <Header>
        {sound ? `Дорожка ${sound?.name}` : "Нет активной дорожки"}
      </Header>
      <audio ref={palyer} style={{ width: "100%" }} controls>
        <source src={`http://localhost:3000${sound?.path}`} type="audio/mpeg" />
      </audio>
    </Segment>
  );
};

const ItemRow = ({ sound, active, onClick = () => {} }) => {
  return (
    <List.Item active={active} style={{ cursor: "pointer" }} onClick={onClick}>
      {active ? (
        <List.Icon name="play circle" size="large" verticalAlign="middle" />
      ) : (
        <List.Icon
          name="play circle outline"
          size="large"
          verticalAlign="middle"
        />
      )}

      <List.Content>
        <List.Header as="a">{sound.name}</List.Header>
        <List.Description as="a">{`${sound.level} - ${sound.name}`}</List.Description>
      </List.Content>
    </List.Item>
  );
};
