const express = require("express");
const fs = require('fs');
const path = require("path")
const app = express();
const PORT = 3000;

const cors = require('cors');
app.use(cors());

app.use("/", express.static('dist'));

app.use("/sounds", express.static('db'));

app.use("/api/sounds", (req, res) => {
    try {
        const levels = fs.readdirSync('./db').map(level => ({
            name: level,
            sounds: fs.readdirSync(`./db/${level}`).map(s => ({
                name: s.split("_")[2].replace(".mp3", ""),
                level: level,
                path: `/sounds/${level}/${s}`
            })).sort((a, b) => a.name.split(".")[0] - b.name.split(".")[0] || a.name.split(".")[1] - b.name.split(".")[1])
        }))
        res.json(levels);
    }
    catch (ex) {
        res.status(500).json({
            message: ex?.message || "Не удалось получить уровни"
        })
    }
});

app.get('*', (req, res) => res.sendFile(path.resolve('dist', 'index.html')));


app.listen(PORT, () => {
    console.log(`Service is listening on port ${PORT}`)
})

